package com.project.ministry.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.project.ministry.activities.MainActivity;
import com.project.ministry.controller.NetworkApiController;
import com.project.pidpool.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by SAG-E-ATTAR on 4/12/2018.
 */

public class NotificationService extends Service {


    public NotificationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);


        final Timer timerObj = new Timer();
        final TimerTask timerTaskObj = new TimerTask() {
            public void run() {
                //perform your action here


                if (NetworkApiController.controller.getCounter()==0)
                {

                    setNotification();

                    int c = NetworkApiController.controller.getCounter();
                    NetworkApiController.controller.setCounter(c+1);

                }
                else if (NetworkApiController.controller.getCounter() == 1)
                {
                    setNextNotification();

                    int c = NetworkApiController.controller.getCounter();
                    NetworkApiController.controller.setCounter(c+1);

                    stopSelf();

                    timerObj.cancel();

                }



            }
        };
        timerObj.schedule(timerTaskObj, 8000, 8000);


//        Log.i("In Way", "Again Started"+ (counter++));


        return NotificationService.START_STICKY;
    }


    public void setNotification()
    {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.small_pin)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.drawable.small_pin))
                        .setContentTitle("Drink Water")
                        .setContentText("You should 1Ltr water now").
                        setVibrate(new long[] { 1000, 1000}).setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setShowWhen(true)
                        .setPriority(Notification.PRIORITY_MAX);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(MainActivity.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

    }
  public void setNextNotification()
    {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.small_pin)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.drawable.small_pin))
                        .setContentTitle("Lunch Break")
                        .setContentText("You can now take lunch hour till 2:00PM").
                        setVibrate(new long[] { 1000, 1000}).setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setShowWhen(true)
                        .setPriority(Notification.PRIORITY_MAX);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(MainActivity.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent restartService = new Intent("RestartService");
        sendBroadcast(restartService);
    }
}
