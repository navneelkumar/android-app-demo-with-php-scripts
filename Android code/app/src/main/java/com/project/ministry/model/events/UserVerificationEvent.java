package com.project.ministry.model.events;

/**
 * Created by SAG-E-ATTAR on 3/4/2018.
 */

public class UserVerificationEvent {

    boolean code = false;
    String message;

    public UserVerificationEvent(boolean code, String message) {
        this.code = code;
        this.message = message;
    }


    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
