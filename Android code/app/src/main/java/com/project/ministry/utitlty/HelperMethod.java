package com.project.ministry.utitlty;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.project.ministry.controller.NetworkApiController;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

/**
 *
 *
 */

public class HelperMethod {

    public static String convertJavaToJson(final Object object){
        Gson gson = new Gson();
        return  gson.toJson(object);
    }

  public static Object convertJsonToJava(final String json){

      Gson gson = new Gson();

      Object requiredObject = gson.fromJson(json,Object.class);

      return requiredObject;
  }


    public static void HideKeyboard(FragmentActivity context)
    {

        View v = context.getCurrentFocus();

        if (v != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

    }

    public static void showRoundImagePreview(final FragmentActivity context,@NonNull String imageUrl, final ImageView imageView) {

        //For rounded image
        Glide.with(context).load(imageUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {

                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);

                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }



    public static void showRoundImagePreviewBitmap(final FragmentActivity context,@NonNull Bitmap bitmap, final ImageView imageView) {

        //For rounded image
        Glide.with(context).load(bitmapToByte(bitmap)).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {

                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);

                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }
    public static byte[] bitmapToByte(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }




    public static void getShowServerImageBitmap(final FragmentActivity context,@NonNull String imageUrl, final ImageView imageView) {


        //For rounded image
        Glide.with(context).load(imageUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {

                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);

                Bitmap bitmap = Bitmap.createBitmap(resource);

                imageView.setImageDrawable(circularBitmapDrawable);

                NetworkApiController.controller.setProfile_image(bitmap);

            }
        });


        return ;
    }

    public static String convertBitmapToString(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);
        return imageEncoded;

    }

    public static Bitmap convertStringToBitmap(String imageString)
    {

        byte[] imageAsBytes = Base64.decode(imageString.getBytes(), Base64.DEFAULT);

        Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

        return bitmap;
    }


    /*******************************************************************************************/
                /* These methods are used for user Verification purposes*/
    /*******************************************************************************************/

    /**
     * These methods are used to get/set flag that either the
     * user Verified or not ?
     * if checked then  returns "true"
     * else "false"
     * @return flag from Shared Preferences
     */
    public static boolean getVerifyFlag(FragmentActivity context) {

        String SharedPreferenceName = "Login.Verified.User.Custom.AbdulQadeerAttari";

        SharedPreferences sharedPreferences;

        sharedPreferences = context.getSharedPreferences(SharedPreferenceName, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("user_verify_flag",false);
    }
    public static void setVerifyFlag(boolean flag,FragmentActivity context) {

        String SharedPreferenceName = "Login.Verified.User.Custom.AbdulQadeerAttari";

        SharedPreferences sharedPreferences;

        sharedPreferences = context.getSharedPreferences(SharedPreferenceName, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("user_verify_flag",flag);
        editor.commit();
    }


    /**
     * For verification purposes
     * These methods are used to get/set json profile
     * if profile exists then  returns "profile"
     * else "null"
     * @return UserName from Shared Preferences
     */
    public static String getLocalProfile(FragmentActivity context) {

        String SharedPreferenceName = "Login.Verified.User.Custom.AbdulQadeerAttari";

        SharedPreferences sharedPreferences;

        sharedPreferences = context.getSharedPreferences(SharedPreferenceName, Context.MODE_PRIVATE);
        return sharedPreferences.getString("user_profile","");
    }

    public static void setLocalProfile(String user_profile,FragmentActivity context){

        String SharedPreferenceName = "Login.Verified.User.Custom.AbdulQadeerAttari";

        SharedPreferences sharedPreferences;

        sharedPreferences = context.getSharedPreferences(SharedPreferenceName, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_profile",user_profile);
        editor.commit();

    }

    /**
     * For store bitmap purposes
     * These methods are used to get/set json profile
     * if profile exists then  returns "profile"
     * else "null"
     * @return UserName from Shared Preferences
     */
    public static String getLocalStringImage(FragmentActivity context) {

        String SharedPreferenceName = "Login.Verified.User.Custom.AbdulQadeerAttari";

        SharedPreferences sharedPreferences;

        sharedPreferences = context.getSharedPreferences(SharedPreferenceName, Context.MODE_PRIVATE);
        return sharedPreferences.getString("user_profile_local_image","");
    }

    public static void setLocalStringImage(String user_profile,FragmentActivity context){

        String SharedPreferenceName = "Login.Verified.User.Custom.AbdulQadeerAttari";

        SharedPreferences sharedPreferences;

        sharedPreferences = context.getSharedPreferences(SharedPreferenceName, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_profile_local_image",user_profile);
        editor.commit();

    }

    /*******************************************************************************************/
    /*******************************************************************************************/


    //Date Picker Dialog
    public static void showDatePickerDialog(FragmentActivity context, final TextView textView)
    {

        final String[] retunDate = new String[1];

        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {


                String year = String.valueOf(selectedYear);
                String month = String.valueOf(selectedMonth+1);
                String day = String.valueOf(selectedDay);

                retunDate[0] = day+"/"+month+"/"+year;

                textView.setText(retunDate[0]);

            }
        }, year, month, day);

        datePickerDialog.show();

    }


    //Time Picker Dialog
    public static void showTimePickerDialog(final FragmentActivity context, final TextView textView)
    {

        final String[] returnTime = new String[1];

        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker timePicker, int hourOFDay, int minuteOfDay) {


                String hour = String.valueOf(hourOFDay);
                String minute = String.valueOf(minuteOfDay);

                returnTime[0] = hour+":"+minute;


                textView.setText(returnTime[0]);

            }
        }, hour, minute, false);

        timePickerDialog.show();

    }

    public static String getDeviceTime()
    {
        //Formatting of date is (DD/MM/YY)

        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(c.get(Calendar.MINUTE));
        String CurrentTime = hour+":"+minute;


        return CurrentTime;
    }



}
