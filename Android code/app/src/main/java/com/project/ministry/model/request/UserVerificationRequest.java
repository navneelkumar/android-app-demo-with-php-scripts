
package com.project.ministry.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserVerificationRequest {

    @SerializedName("u_id")
    @Expose
    private String uId;
    @SerializedName("u_code")
    @Expose
    private String uCode;

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

    public String getUCode() {
        return uCode;
    }

    public void setUCode(String uCode) {
        this.uCode = uCode;
    }

}
