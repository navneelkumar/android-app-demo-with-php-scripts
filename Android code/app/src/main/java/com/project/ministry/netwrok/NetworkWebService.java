package com.project.ministry.netwrok;

import com.project.ministry.model.request.GetProfileRequest;
import com.project.ministry.model.request.LoginRequest;
import com.project.ministry.model.request.UpdateProfileRequest;
import com.project.ministry.model.request.UserVerificationRequest;
import com.project.ministry.netwrok.response.getProfile.GetProfileResponce;
import com.project.ministry.netwrok.response.login.LoginResponce;
import com.project.ministry.netwrok.response.userVerification.UserVerificationResponce;
import com.project.ministry.netwrok.updateprofile.UpdateProfileResponce;
import com.project.ministry.netwrok.response.signup.SignupResponce;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 */
public interface NetworkWebService {

    @POST("u_login.php")
    Call<LoginResponce> login(@Body LoginRequest body);

    @Multipart
    @POST("signup.php")
    Call<SignupResponce> signUp(@Part MultipartBody.Part file , @Part("json") RequestBody json);


    @POST("u_verification.php")
    Call<UserVerificationResponce> user_verification(@Body UserVerificationRequest body);


    @POST("getprofile.php")
    Call<GetProfileResponce> get_Profile(@Body GetProfileRequest body);


    @POST("updateprofile.php")
    Call<UpdateProfileResponce> update_profile(@Body UpdateProfileRequest body);


}