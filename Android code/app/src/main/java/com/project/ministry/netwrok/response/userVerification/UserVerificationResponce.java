
package com.project.ministry.netwrok.response.userVerification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserVerificationResponce {

    @SerializedName("status")
    @Expose
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
