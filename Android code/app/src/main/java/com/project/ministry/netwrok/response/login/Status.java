
package com.project.ministry.netwrok.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("code")
    @Expose
    private Integer code;

    @SerializedName("u_id")
    @Expose
    private Integer u_id;

    @SerializedName("u_code")
    @Expose
    private Integer u_code;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getU_code() {
        return u_code;
    }

    public void setU_code(Integer u_code) {
        this.u_code = u_code;
    }

    public Integer getU_id() {
        return u_id;
    }

    public void setU_id(Integer u_id) {
        this.u_id = u_id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
