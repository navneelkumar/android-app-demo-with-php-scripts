package com.project.ministry.utitlty;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.project.pidpool.classes.Drivers;
import com.project.pidpool.classes.Subscription;
import com.project.ministry.netwrok.response.getProfile.Detail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SAG-E-ATTAR on 3/31/2018.
 */

public class Controller {

    //User Profile Object
    Detail userProfileObject;

    //User Profile Image
    Bitmap profile_image ;

    //Used for Maps Activity
    boolean alreadyLoggedIn = true;

    //Used to store temp data
    List<Subscription> subscriptionList = new ArrayList<>();

    //Used to store temp Data
    boolean track = false;
    List<LatLng> latLngList = new ArrayList<>();


    //Tap target
    boolean tapTarget = false;


    //Used in demo Ministry
    int counter = 0;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean isTapTarget() {
        return tapTarget;
    }

    public void setTapTarget(boolean tapTarget) {
        this.tapTarget = tapTarget;
    }

    public boolean isTrack() {
        return track;
    }

    public void setTrack(boolean track) {
        this.track = track;
    }

    public List<LatLng> getLatLngList() {
        return latLngList;
    }

    public void setLatLngList(List<LatLng> latLngList) {
        this.latLngList = latLngList;
    }

    public List<Subscription> getSubscriptionList() {
        return subscriptionList;
    }

    public void setSubscriptionList(List<Subscription> subscriptionList) {
        this.subscriptionList = subscriptionList;
    }

    public Bitmap getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(Bitmap profile_image) {
        this.profile_image = profile_image;
    }

    public boolean isAlreadyLoggedIn() {
        return alreadyLoggedIn;
    }

    public void setAlreadyLoggedIn(boolean alreadyLoggedIn) {
        this.alreadyLoggedIn = alreadyLoggedIn;
    }

    public Detail getUserProfileObject() {
        return userProfileObject;
    }

    public void setUserProfileObject(Detail userProfileObject) {
        this.userProfileObject = userProfileObject;
    }
}
