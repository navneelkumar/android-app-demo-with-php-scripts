
package com.project.ministry.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProfileRequest {

    @SerializedName("u_id")
    @Expose
    private String uId;

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

}
