package com.project.ministry.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project.ministry.controller.NetworkApiController;
import com.project.ministry.utitlty.HelperMethod;
import com.project.pidpool.R;
import com.project.ministry.model.events.UserVerificationEvent;
import com.project.ministry.utitlty.Checks;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class VerificationActivity extends AppCompatActivity {


    EditText et_verification_code;

    Button bt_verification;

    ProgressDialog progressDialog;

    private NetworkApiController mNetworkApiController = null;

    String ver_code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        et_verification_code = findViewById(R.id.et_verification_code);

        bt_verification = findViewById(R.id.bt_verification);

        progressDialog = new ProgressDialog(VerificationActivity.this);

        mNetworkApiController = NetworkApiController.getInstance(VerificationActivity.this);

        Intent myIntent = getIntent();

        ver_code = myIntent.getStringExtra("digit");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                addNotification(ver_code);

                Toast.makeText(VerificationActivity.this, "Check Notification", Toast.LENGTH_SHORT).show();

            }
        },3000);
    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);


    }
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        bt_verification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String ver_code = et_verification_code.getText().toString();


                if (Checks.isInternetOn(VerificationActivity.this))
                {

                    int length =ver_code.length();

                    if (ver_code.equalsIgnoreCase("")||
                            length<4) {

                        Toast.makeText(VerificationActivity.this, "4 digit code required", Toast.LENGTH_SHORT).show();


                    }
                    else
                    {
                        progressDialog.setTitle("Authenticating");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        mNetworkApiController.user_verification(ver_code);

                    }
                }
                else
                {
                    Checks.showAlert(VerificationActivity.this,"No Internet Connection","OK");

                }

            }
        });
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVerificationResponse(UserVerificationEvent event)
    {

        if (progressDialog.isShowing())
            progressDialog.dismiss();

        if (event.isCode()) // (code is 200 / success)
        {

            startActivity(new Intent(VerificationActivity.this,MapsActivity.class));
            finish();
            Toast.makeText(this, "Logged In", Toast.LENGTH_SHORT).show();

            //Setting flag to SharedPreferences (User is verified)true
            HelperMethod.setVerifyFlag(true,VerificationActivity.this);

            //Used in Maps Activity
            NetworkApiController.controller.setAlreadyLoggedIn(false);


        }
        else  // (code is 400 / failure)
        {

            Toast.makeText(this, event.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }

    private void addNotification(String digitcode) {


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.small_pin)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.drawable.small_pin))
                        .setContentTitle("Verification Code")
                        .setContentText(digitcode).
                        setVibrate(new long[] { 1000, 1000}).setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setShowWhen(true)
                        .setPriority(Notification.PRIORITY_MAX);


//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(VerificationActivity.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }



}
