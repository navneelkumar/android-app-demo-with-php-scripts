package com.project.ministry.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.project.pidpool.R;

public class AboutActivity extends AppCompatActivity {


    LinearLayout about_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


        about_back = findViewById(R.id.about_back);



    }

    @Override
    protected void onResume() {
        super.onResume();


        about_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }
}
