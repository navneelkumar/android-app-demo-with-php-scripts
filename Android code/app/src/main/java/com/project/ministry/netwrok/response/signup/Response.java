
package com.project.ministry.netwrok.response.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("u_code")
    @Expose
    private String uCode;


    public String getuCode() {
        return uCode;
    }

    public void setuCode(String uCode) {
        this.uCode = uCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUCode() {
        return uCode;
    }

    public void setUCode(String uCode) {
        this.uCode = uCode;
    }

}
