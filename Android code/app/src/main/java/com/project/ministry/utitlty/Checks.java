package com.project.ministry.utitlty;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;


import com.project.ministry.activities.MainActivity;
import com.project.ministry.controller.NetworkApiController;
import com.project.pidpool.R;

import java.util.Calendar;

/**
 * Created by SAG-E-ATTAR on 8/18/2017.
 */

public class Checks {



    //For internet connection checking
    public static final boolean isInternetOn(Context context) {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

            // if connected with internet

            //  Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

            //if not connected with internet
        } else if (

                connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

            // Toast.makeText(this, "Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    public static void showAlert(Context context,String message, String btn)
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
       // builder1.setTitle("No Internet");
       // builder1.setIcon(R.mipmap.ic_launcher);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                btn,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public static void showInternetAlert(Context context)
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
       // builder1.setTitle("No Internet");
       // builder1.setIcon(R.mipmap.ic_launcher);
        builder1.setMessage("No Internet Connection");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public static void LogoutAlert(final FragmentActivity context, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Logout");
        builder.setMessage(message);
        builder.setIcon(R.mipmap.alertshow);
        builder.setCancelable(false);
        builder.setPositiveButton( "YES",
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        context.startActivity(new Intent(context, MainActivity.class));

                        HelperMethod.setVerifyFlag(false,context);

                        NetworkApiController.controller.setProfile_image(null);
                        NetworkApiController.controller.setUserProfileObject(null);

                        Toast.makeText(context, "You are logged out !!!", Toast.LENGTH_SHORT).show();

                    }

                });

        builder.setNegativeButton( "NO",
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();

                    }

                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    public static String  DeviceTime()
    {

        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(c.get(Calendar.MINUTE));
        String second = String.valueOf(c.get(Calendar.SECOND));
        String CurrentTime = hour+":"+minute+":"+second;
        // Use the current date as the default date in the picker
        final Calendar d = Calendar.getInstance();
        String year = String.valueOf(d.get(Calendar.YEAR));
        String month = String.valueOf(d.get(Calendar.MONTH));
        String day = String.valueOf(d.get(Calendar.DAY_OF_MONTH));
        String CurrentDate = year+":"+month+":"+day;

        String deviceTime = CurrentDate+" "+CurrentTime;

        return deviceTime;
    }




}
