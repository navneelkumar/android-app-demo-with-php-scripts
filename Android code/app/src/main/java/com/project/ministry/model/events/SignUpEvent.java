package com.project.ministry.model.events;

/**
 * Created by SAG-E-ATTAR on 3/4/2018.
 */

public class SignUpEvent {

    boolean code = false;
    String message;

    Integer user_id;
    String user_code;

    public SignUpEvent(boolean code, String message, Integer user_id, String user_code) {
        this.code = code;
        this.message = message;
        this.user_id = user_id;
        this.user_code = user_code;
    }

    public SignUpEvent(boolean code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
