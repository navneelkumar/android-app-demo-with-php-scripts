package com.project.ministry.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.project.ministry.utitlty.HelperMethod;
import com.project.pidpool.R;


public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final boolean isVerified = HelperMethod.getVerifyFlag(Splash.this);

        new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//
//                if (isVerified)
//                {

//                    startActivity(new Intent(Splash.this, MapsActivity.class));
//                    finish();

//                }
//                else
//                {

                    startActivity(new Intent(Splash.this, MainActivity.class));
                    finish();

//                }


            }
        },2000);


    }

    @Override
    protected void onResume() {
        super.onResume();




    }


}
