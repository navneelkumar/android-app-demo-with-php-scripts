package com.project.ministry.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.project.ministry.controller.NetworkApiController;
import com.project.pidpool.R;
import com.project.ministry.model.events.SignUpEvent;
import com.project.ministry.utitlty.Checks;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class RegistrationActivity extends AppCompatActivity {


    public static final int REQUEST_CODE_IMAGE_CAPTURE = 0x1001;

    ImageView iv_register_image;

    EditText et_register_username,et_register_password,et_register_number,et_register_email,et_register_address
            ,et_register_cnic;

    Button bt_register,btn_login_now;

    File compressedImageFile;

    ProgressDialog progressDialog;

    private NetworkApiController mNetworkApiController = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new);

        iv_register_image = findViewById(R.id.iv_register_image);

        et_register_username = findViewById(R.id.et_register_username);
        et_register_password = findViewById(R.id.et_register_password);
        et_register_number = findViewById(R.id.et_register_number);
        et_register_email = findViewById(R.id.et_register_email);
        et_register_address = findViewById(R.id.et_register_address);
        et_register_cnic = findViewById(R.id.et_register_cnic);
        btn_login_now = findViewById(R.id.btn_login_now);



        bt_register = findViewById(R.id.bt_register);

        progressDialog = new ProgressDialog(RegistrationActivity.this);

        mNetworkApiController = NetworkApiController.getInstance(RegistrationActivity.this);


        btn_login_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(RegistrationActivity.this,LoginActivity.class));
                finish();
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);


    }
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }
    @Override
    protected void onResume() {
        super.onResume();


        bt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(Checks.isInternetOn(RegistrationActivity.this)) {

                    if (!et_register_username.getText().toString().equalsIgnoreCase("") &&
                            !et_register_password.getText().toString().equalsIgnoreCase("") &&
                            !et_register_number.getText().toString().equalsIgnoreCase("") &&
                            !et_register_email.getText().toString().equalsIgnoreCase("") &&
                            !et_register_address.getText().toString().equalsIgnoreCase("") &&
                            !et_register_cnic.getText().toString().equalsIgnoreCase("")) {

                        File imageFile = compressedImageFile;


                        if (imageFile != null) {

                            progressDialog.setTitle("Authenticating");
                            progressDialog.setCancelable(false);
                            progressDialog.show();

                            mNetworkApiController.signup(imageFile, et_register_username.getText().toString(),
                                    et_register_number.getText().toString(), et_register_password.getText().toString(),
                                    et_register_email.getText().toString(), et_register_address.getText().toString(),
                                    et_register_cnic.getText().toString());

                        } else {

                            Toast.makeText(RegistrationActivity.this, "Image Missing", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(RegistrationActivity.this, "Incomplete Info.", Toast.LENGTH_SHORT).show();

                    }
                }
                else
                {

                    Checks.showInternetAlert(RegistrationActivity.this);
                }

            }
        });


        iv_register_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                callLocalerPermission();

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignupResponse(final SignUpEvent event)
    {

        if (progressDialog.isShowing())
            progressDialog.dismiss();

        
        if (event.isCode()) // (code is 200 / success)
        {

            Toast.makeText(RegistrationActivity.this, event.getMessage(), Toast.LENGTH_SHORT).show();

            et_register_username.setText("");
            et_register_password.setText("");
            et_register_number.setText("");
            et_register_email.setText("");
            et_register_address.setText("");
            et_register_cnic.setText("");


            startActivity(new Intent(RegistrationActivity.this,VerificationActivity.class).putExtra("digit",event.getUser_code()));
            finish();

            Uri placeholderUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                    "://" + getResources().getResourcePackageName(R.mipmap.signup_camera)
                    + '/' + getResources().getResourceTypeName(R.mipmap.signup_camera)
                    + '/' + getResources().getResourceEntryName(R.mipmap.signup_camera) );

            showImagePreview(String.valueOf(placeholderUri),iv_register_image);

        }
        else  // (code is 400 / failure)
        {

            Toast.makeText(RegistrationActivity.this, event.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }


    // For Camera Purposes and save image to gallery
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_IMAGE_CAPTURE)
                onCaptureImageResult(data);

        }
    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            compressedImageFile =destination;

            showImagePreview(destination.getAbsolutePath(),iv_register_image);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void showImagePreview(@NonNull String imageUrl, final ImageView imageView) {

        //For rounded image
        Glide.with(RegistrationActivity.this).load(imageUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {

                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);

                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }


    PermissionListener permissionlistenerLocaller = new PermissionListener() {
        @Override
        public void onPermissionGranted() {

            try {

                cameraIntent();

            }catch (Exception exp)
            {

            }


        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }


    };

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_FRONT);
        startActivityForResult(intent, REQUEST_CODE_IMAGE_CAPTURE);
    }

    private void callLocalerPermission(){

        new TedPermission(RegistrationActivity.this)
                .setPermissionListener(permissionlistenerLocaller)
                .setDeniedMessage("If you reject permission,you can not use this service\n\n Please turn on permissions at [Setting] > [Permission]" )
                .setPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .check();
    }



}
