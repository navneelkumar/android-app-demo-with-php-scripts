package com.project.ministry.activities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.project.ministry.controller.NetworkApiController;
import com.project.ministry.model.events.UserProfileEvent;
import com.project.ministry.utitlty.HelperMethod;
import com.project.pidpool.R;
import com.project.ministry.netwrok.response.getProfile.Detail;
import com.project.ministry.services.NotificationService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.toptas.fancyshowcase.FancyShowCaseView;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class MapsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ImageView drawerImage;

    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;

    TextView drawerUsername;

    String placeholderUri;


    LinearLayout profile_back;

    ImageView iv_view_profile_image;

    TextView et_view_profile_username,et_view_profile_phone,et_view_profile_email,et_view_profile_address,et_view_profile_cnic;

    Button bt_view_profile;

    Detail profileObject;

    Bitmap profileImage;

    private FloatingActionMenu fam;
    private com.github.clans.fab.FloatingActionButton fab_add_subscription, fab_view_Subscriptions;
    private FloatingActionButton fab_tap_target;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab_view_Subscriptions =  findViewById(R.id.fab_view_Subscriptions);
        fab_add_subscription = findViewById(R.id.fab_add_subscription);
        fam =  findViewById(R.id.fab_menu);
        fab_tap_target =  findViewById(R.id.fab_tap_target);



        et_view_profile_username = findViewById(R.id.et_view_profile_username);
        et_view_profile_phone = findViewById(R.id.et_view_profile_phone);
        et_view_profile_email = findViewById(R.id.et_view_profile_email);
        et_view_profile_address = findViewById(R.id.et_view_profile_address);
        et_view_profile_cnic = findViewById(R.id.et_view_profile_cnic);

        iv_view_profile_image = findViewById(R.id.iv_view_profile_image);
        bt_view_profile = findViewById(R.id.bt_view_profile);

        profileObject = NetworkApiController.controller.getUserProfileObject();

        profileImage = NetworkApiController.controller.getProfile_image();



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        View headerView = navigationView.getHeaderView(0);
        drawerImage = (ImageView) headerView.findViewById(R.id.drawer_user_image);
        drawerUsername = (TextView) headerView.findViewById(R.id.drawer_user_name);

        placeholderUri = ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + getResources().getResourcePackageName(R.drawable.profilereplacer)
                + '/' + getResources().getResourceTypeName(R.drawable.profilereplacer)
                + '/' + getResources().getResourceEntryName(R.drawable.profilereplacer);


        drawerUsername.setText("Abdul Qadeer Attari");
        navigationView.setNavigationItemSelectedListener(this);




        //handling menu status (open or close)
        fam.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened) {
//                    Toast.makeText(getActivity(), "Menu is opened", Toast.LENGTH_SHORT).show();
                    fab_add_subscription.setVisibility(View.VISIBLE);
                    fab_view_Subscriptions.setVisibility(View.VISIBLE);


                }
                else
                {
                    fab_add_subscription.setVisibility(View.GONE);
                    fab_view_Subscriptions.setVisibility(View.GONE);
//                    Toast.makeText(getActivity(), "Menu is closed", Toast.LENGTH_SHORT).show();

                }
            }
        });

        //handling each floating action button clicked

        fab_add_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (fam.isOpened()) {

                    fam.close(true);


                }

            }
        });
        fab_view_Subscriptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(MapsActivity.this,EditProfileActivity.class));

                if (fam.isOpened()) {

                    fam.close(true);


                }

            }
        });

        fam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fam.isOpened()) {

                    fam.close(true);


                }
                else
                {

//                    fabImport.setVisibility(View.VISIBLE);
//                    fabMaual.setVisibility(View.VISIBLE);

                }
            }
        });

        //Showing Targets on home

        if (!NetworkApiController.controller.isTapTarget())
        {

            View toolbar_toogle_View = toolbar.getChildAt(1);

            tapFabTarget(fab_tap_target,toolbar_toogle_View);

            NetworkApiController.controller.setTapTarget(true);
        }

        if ((ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_NOTIFICATION_POLICY) != PackageManager.PERMISSION_GRANTED)) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_NOTIFICATION_POLICY},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {

            // user already provided permission
            // perform function for what you want to achieve

            startService(new Intent(this,NotificationService.class));

        }
    }
    public void tapFabTarget(final View view, final View nextView)
    {



            new MaterialTapTargetPrompt.Builder(MapsActivity.this)
                    .setTarget(view.findViewById(R.id.fab_tap_target))
                    .setPrimaryText("Manage your profile")
                    .setSecondaryText("Tap the button to manage profile")
                    .setBackgroundColour(getResources().getColor(R.color.materialColor))
                    .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener()
                    {
                        @Override
                        public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state)
                        {
                            if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED)
                            {
                                // User has pressed the prompt target

                                tapIconTarget(nextView);

                            }
                            else if (state == MaterialTapTargetPrompt.STATE_DISMISSED)
                            {

                                // User has pressed outside the target

                                tapIconTarget(nextView);

                            }
                        }
                    })
                    .show();


    }

    public void tapIconTarget(View view)
    {
        new FancyShowCaseView.Builder(MapsActivity.this)
                .focusOn(view)
                .title("Tap icon to get menu")
                .fitSystemWindows(false)
                .build()
                .show();

    }


    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);


    }

    /*Ending the updates for the location service*/
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        boolean canUseExternalStorage = false;

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    canUseExternalStorage = true;
                }

                if (!canUseExternalStorage) {
                    Toast.makeText(MapsActivity.this, "Cannot use this feature without requested permission", Toast.LENGTH_SHORT).show();
                } else {
                    // user now provided permission
                    // perform function for what you want to achieve

                    startService(new Intent(this,NotificationService.class));

                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getSupportActionBar().setTitle("My Profile");


        if (NetworkApiController.controller.isAlreadyLoggedIn()) { //true

            //Already Logged In
            //Profile Object is not set to Controller

            String localStringImage = HelperMethod.getLocalStringImage(MapsActivity.this);

            if (!localStringImage.equalsIgnoreCase("")) {
                Bitmap bitmap = HelperMethod.convertStringToBitmap(localStringImage);
                NetworkApiController.controller.setProfile_image(bitmap);
                HelperMethod.showRoundImagePreviewBitmap(MapsActivity.this, bitmap, drawerImage);
            }

            String localUserProfile = HelperMethod.getLocalProfile(MapsActivity.this);

            //Decoding Json
            Gson gson = new Gson();
            Detail userProfileObject = gson.fromJson(localUserProfile, Detail.class);


            NetworkApiController.controller.setUserProfileObject(userProfileObject);


            //Setting username to drawer
            drawerUsername.setText(userProfileObject.getUsername());

        } else // LoggedIn / Sign up from Verification
        { // false

            //New Logged In
            //Profile Object is already set to Controller


        }

        bt_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MapsActivity.this,EditProfileActivity.class));


            }
        });

        if (profileImage!=null)
        {

            HelperMethod.showRoundImagePreviewBitmap(MapsActivity.this,profileImage,iv_view_profile_image);
        }

        if (profileObject != null)
        {

            if (profileObject.getUsername() != null)
                et_view_profile_username.setText(profileObject.getUsername());

            if (profileObject.getUserPhone() != null)
                et_view_profile_phone.setText(profileObject.getUserPhone());

            if (profileObject.getAddress() != null)
                et_view_profile_address.setText(profileObject.getAddress());

            if (profileObject.getUserEmail() != null)
                et_view_profile_email.setText(profileObject.getUserEmail());

            if (profileObject.getCnicNumber() != null)
                et_view_profile_cnic.setText(profileObject.getCnicNumber());


        }
        else
        {
//            Toast.makeText(this, "Null Profile", Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {

            //This is profile

        } else if (id == R.id.nav_add_subscription) {

            startActivity(new Intent(MapsActivity.this,AddSubscriptionActivity.class));


        }else if (id == R.id.nav_subscriptions) {

            startActivity(new Intent(MapsActivity.this,MySubscribtionActivity.class));


        } else if (id == R.id.nav_settings) {

            startActivity(new Intent(MapsActivity.this,EditProfileActivity.class));

        } else if (id == R.id.nav_about)
        {

            startActivity(new Intent(MapsActivity.this,AboutActivity.class));


        } else if (id == R.id.nav_logout) {


            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserProfileResponse(final UserProfileEvent event) {

        if (event.isCode()) // (code is 200 / success)
        {

            Detail userProfile = event.getUserDetailObject();

            //Setting User Profile Object in Controller / Cache
            NetworkApiController.controller.setUserProfileObject(userProfile);

            String imageUri = userProfile.getImage_uri();

            String json = HelperMethod.convertJavaToJson(userProfile);

            //Saved locally Profile in Shared Preferences
            HelperMethod.setLocalProfile(json, MapsActivity.this);


            drawerUsername.setText(event.getUserDetailObject().getUsername());

            et_view_profile_username.setText(event.getUserDetailObject().getUsername());
            et_view_profile_phone.setText(event.getUserDetailObject().getUserPhone());
            et_view_profile_email.setText(event.getUserDetailObject().getUserEmail());
            et_view_profile_address.setText(event.getUserDetailObject().getAddress());
            et_view_profile_cnic.setText(event.getUserDetailObject().getCnicNumber());

            if (imageUri != null) {

                if (!imageUri.equalsIgnoreCase("")) {

                    //Loading image from server and getting as bitmap
                    HelperMethod.getShowServerImageBitmap(MapsActivity.this, imageUri, drawerImage);

                    HelperMethod.getShowServerImageBitmap(MapsActivity.this, imageUri, iv_view_profile_image);



                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            Bitmap bitmap = NetworkApiController.controller.getProfile_image();

                            if (bitmap != null) {

                                //Converting bitmap into byte array
                                String userImageConvertedString = HelperMethod.convertBitmapToString(bitmap);

                                //Saving byte array of bitmap into local storage
                                HelperMethod.setLocalStringImage(userImageConvertedString, MapsActivity.this);
                            }

                        }
                    }, 3000);

                }

            }

        } else  // (code is 400 / failure)
        {

            Toast.makeText(this, "Profile Loading Failed", Toast.LENGTH_SHORT).show();

        }

    }

}
