package com.project.ministry.controller;

import android.content.Context;
import android.os.Process;

import com.google.gson.Gson;
import com.project.ministry.model.events.LoginEvent;
import com.project.ministry.model.events.SignUpEvent;
import com.project.ministry.model.events.UpdateProfileEvent;
import com.project.ministry.model.events.UserProfileEvent;
import com.project.ministry.model.events.UserVerificationEvent;
import com.project.ministry.model.request.GetProfileRequest;
import com.project.ministry.model.request.LoginRequest;
import com.project.ministry.model.request.SignupRequest;
import com.project.ministry.model.request.UpdateProfileRequest;
import com.project.ministry.model.request.UserVerificationRequest;
import com.project.ministry.netwrok.NetworkClient;
import com.project.ministry.netwrok.response.getProfile.Detail;
import com.project.ministry.netwrok.response.getProfile.GetProfileResponce;
import com.project.ministry.netwrok.response.login.LoginResponce;
import com.project.ministry.netwrok.response.signup.SignupResponce;
import com.project.ministry.netwrok.response.userVerification.UserVerificationResponce;
import com.project.ministry.netwrok.updateprofile.UpdateProfileResponce;
import com.project.ministry.utitlty.Controller;


import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aaa on 8/30/2016.
 */
public class NetworkApiController implements Runnable {

    private static final String TAG = NetworkApiController.class.getCanonicalName();


    public static Controller controller = new Controller();

    private static NetworkApiController inst = null;
    private Context mContext = null;
    private Thread mThread = null;

    //Executes services i.e: as processes in operating systems
    private final ExecutorService threadPool = Executors.newCachedThreadPool();

    private NetworkClient mNetworkClient = null;
   // private DataBaseController mDataBaseController = null;

    private EventBus mEventBus = EventBus.getDefault();

    String received_user_id;


    private NetworkApiController(Context pContext) {

        mContext = pContext;
        mNetworkClient = NetworkClient.getClient();
        mThread = new Thread(this);
        mThread.setName("WebController");
        mThread.start();
     //   mDataBaseController = DataBaseController.getInstance(mContext);
    }




    //U
    public synchronized static NetworkApiController getInstance(Context pContext) {
        if (inst == null) {
            inst = new NetworkApiController(pContext);
        }
        return inst;
    }


    @Override
    public void run() {
        try {

            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        } catch (Exception e) {
//            LogUtil.error(TAG,
//                    "Error in WebController run and message is ="
//                            + e.getMessage());
        }
    }


    public synchronized void login(final String userName , final String phone) {

        threadPool.execute(new Runnable() {
            @Override
            public void run() {

                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setUsername(userName);
                loginRequest.setUserPhone(phone);

//                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (HelperMethod.convertJavaToJson(postSignUp).toString()));
                Call<LoginResponce> call = mNetworkClient.getNetworkWebService().login(loginRequest);



//                   There are two other ways to send request
//
//                   1.
//
//                   RequestBody body = new FormBody.Builder().add("username",userName).add("password",password).build;
//                    Call<StatusResponse> call = mNetworkClient.getNetworkWebService().login(body);
//
//                ************************************************************************************************
//
//                    2.
//                        Directly sending request object instead of okhttp3 request body
//
//                    i.e:
//
//                    Call<StatusResponse> call = mNetworkClient.getNetworkWebService().login(postSignUp);
//
//
//
//
//

                call.enqueue(new Callback<LoginResponce>() {
                    @Override
                    public void onResponse(Call<LoginResponce> call, Response<LoginResponce> response) {
                        if (response.isSuccessful() && response != null) {
                            LoginResponce webResponse = response.body();
                            com.project.ministry.netwrok.response.login.Status status = webResponse.getStatus();

                            if (status.getCode() == 200) {

                                received_user_id = String.valueOf(status.getU_id());

                                String user_code = String.valueOf(status.getU_code());

                                mEventBus.post(new LoginEvent(true,status.getMessage(),user_code));


                            }else {

                                mEventBus.post(new LoginEvent(false,status.getMessage()));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponce> call, Throwable t) {

                        mEventBus.post(new LoginEvent(false,t.getMessage()));

                    }
                });

            }
        });
    }

    public synchronized void signup(final File imageFile , final String userName , final String phone, final String password,
                                      final String email, final  String address, final String cnic) {

        threadPool.execute(new Runnable() {
            @Override
            public void run() {

                SignupRequest signupRequest = new SignupRequest();
               signupRequest.setUsername(userName);
               signupRequest.setUserPassword(password);
               signupRequest.setUserPhone(phone);
               signupRequest.setUserEmail(email);
               signupRequest.setAddress(address);
               signupRequest.setCnicNumber(cnic);

                Gson gson= new Gson();
                String Json = gson.toJson(signupRequest);

                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), imageFile);
                MultipartBody.Part upload = MultipartBody.Part.createFormData("file", imageFile.getName(), reqFile);
                RequestBody json = RequestBody.create(MediaType.parse("text/plain"), Json);


                Call<SignupResponce> call = mNetworkClient.getNetworkWebService().signUp(upload,json);


                call.enqueue(new Callback<SignupResponce>() {
                    @Override
                    public void onResponse(Call<SignupResponce> call, Response<SignupResponce> response) {
                        if (response.isSuccessful() && response != null) {
                            SignupResponce webResponse = response.body();
                            com.project.ministry.netwrok.response.signup.Status status = webResponse.getStatus();

                            com.project.ministry.netwrok.response.signup.Response response1 =webResponse.getResponse();

                            if (status.getCode() == 200) {

                                received_user_id = String.valueOf(response1.getUserId());

                                mEventBus.post(new SignUpEvent(true,status.getMessage(),response1.getUserId(),response1.getUCode()));

                            }else {

                                mEventBus.post(new SignUpEvent(false,status.getMessage()));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SignupResponce> call, Throwable t) {

                        mEventBus.post(new SignUpEvent(false,t.getMessage()));

                    }
                });

            }
        });
    }


    public synchronized void user_verification(final String user_code) {

        threadPool.execute(new Runnable() {
            @Override
            public void run() {

                UserVerificationRequest userVerificationRequest = new UserVerificationRequest();
                userVerificationRequest.setUCode(user_code);
                userVerificationRequest.setUId(received_user_id);


                Call<UserVerificationResponce> call = mNetworkClient.getNetworkWebService().user_verification(userVerificationRequest);


                call.enqueue(new Callback<UserVerificationResponce>() {
                    @Override
                    public void onResponse(Call<UserVerificationResponce> call, Response<UserVerificationResponce> response) {
                        if (response.isSuccessful() && response != null) {
                            UserVerificationResponce webResponse = response.body();
                            Status status = webResponse.getStatus();

                            if (status.getCode() == 200) {

                                getUserProfile();

                                mEventBus.post(new UserVerificationEvent(true,status.getMessage()));

                            }else {

                                mEventBus.post(new UserVerificationEvent(false,status.getMessage()));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UserVerificationResponce> call, Throwable t) {

                        mEventBus.post(new UserVerificationEvent(false,t.getMessage()));

                    }
                });

            }
        });
    }

    public synchronized void getUserProfile() {

        threadPool.execute(new Runnable() {
            @Override
            public void run() {

                GetProfileRequest getProfileRequest = new GetProfileRequest();
                getProfileRequest.setUId(received_user_id);


                Call<GetProfileResponce> call = mNetworkClient.getNetworkWebService().get_Profile(getProfileRequest);


                call.enqueue(new Callback<GetProfileResponce>() {
                    @Override
                    public void onResponse(Call<GetProfileResponce> call, Response<GetProfileResponce> response) {
                        if (response.isSuccessful() && response != null) {
                            GetProfileResponce webResponse = response.body();

                            com.project.ministry.netwrok.response.getProfile.Status status = webResponse.getStatus();

                            com.project.ministry.netwrok.response.getProfile.Response profile_response = webResponse.getResponse();

                            Detail user_detail = profile_response.getDetail().get(0);

                            if (status.getCode() == 200) {

                                String image_uri = "http://iryaanrentacar.com/ministry/pics/"+user_detail.getImage_uri();

                                Detail jsonUserDetail = new Detail();

                                jsonUserDetail = user_detail;

                                jsonUserDetail.setImage_uri(image_uri);

                                mEventBus.post(new UserProfileEvent(true,jsonUserDetail));


//                                String json = HelperMethod.convertJavaToJson(jsonUserDetail);

//                                controller.setLocalJsonProfile(json);


                            }
                            else
                            {

                                mEventBus.post(new UserProfileEvent(false));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetProfileResponce> call, Throwable t) {

                        mEventBus.post(new UserProfileEvent(false));

                    }
                });

            }
        });
    }



    public synchronized void updateUserProfile(final String userId,final String username,final String userPhone, final String userAddress) {

        threadPool.execute(new Runnable() {
            @Override
            public void run() {

                UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
                updateProfileRequest.setUId(userId);
                updateProfileRequest.setUsername(username);
                updateProfileRequest.setPhonenumber(userPhone);
                updateProfileRequest.setAddress(userAddress);


                Call<UpdateProfileResponce> call = mNetworkClient.getNetworkWebService().update_profile(updateProfileRequest);


                call.enqueue(new Callback<UpdateProfileResponce>() {
                    @Override
                    public void onResponse(Call<UpdateProfileResponce> call, Response<UpdateProfileResponce> response) {
                        if (response.isSuccessful() && response != null)
                        {
                            UpdateProfileResponce webResponse = response.body();

                            com.project.ministry.netwrok.updateprofile.Status status = webResponse.getStatus();


                            if (status.getCode() == 200) {


                                mEventBus.post(new UpdateProfileEvent(true,status.getMessage()));


                            }
                            else
                            {

                                mEventBus.post(new UpdateProfileEvent(false,status.getMessage()));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateProfileResponce> call, Throwable t) {

                        mEventBus.post(new UpdateProfileEvent(false,t.getMessage()));

                    }
                });

            }
        });
    }

}
