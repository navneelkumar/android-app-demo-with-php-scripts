package com.project.ministry.model.events;

import com.project.ministry.netwrok.response.getProfile.Detail;

/**
 * Created by SAG-E-ATTAR on 4/1/2018.
 */

public class UserProfileEvent {

    boolean code ;

    Detail userDetailObject ;

    public UserProfileEvent(boolean code, Detail userDetailObject) {
        this.code = code;
        this.userDetailObject = userDetailObject;
    }

    public UserProfileEvent(boolean code) {
        this.code = code;
    }

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public Detail getUserDetailObject() {
        return userDetailObject;
    }

    public void setUserDetailObject(Detail userDetailObject) {
        this.userDetailObject = userDetailObject;
    }
}
