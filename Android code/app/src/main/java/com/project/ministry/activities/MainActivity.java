package com.project.ministry.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;

import com.easyandroidanimations.library.Animation;
import com.easyandroidanimations.library.AnimationListener;
import com.easyandroidanimations.library.FadeInAnimation;
import com.project.pidpool.R;

public class MainActivity extends AppCompatActivity {

    Button btnSignIn, btnRegister;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSignIn = findViewById(R.id.btnSignIn);
        btnRegister = findViewById(R.id.btnRegister);

        View view = findViewById(R.id.mainActivity);

        new FadeInAnimation(view)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(500)
                .setListener(new AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {


                    }
                })
                .animate();



    }

    private void setupWindowAnimations() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              startActivity(new Intent(MainActivity.this,RegistrationActivity.class));

            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity( new Intent(MainActivity.this,LoginActivity.class));

            }
        });



    }

}
