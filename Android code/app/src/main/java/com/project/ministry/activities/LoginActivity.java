package com.project.ministry.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.project.ministry.controller.NetworkApiController;
import com.project.pidpool.R;
import com.project.ministry.model.events.LoginEvent;
import com.project.ministry.utitlty.Checks;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LoginActivity extends AppCompatActivity {


    EditText userName , userPhone;

    Button btLogin;

    private NetworkApiController mNetworkApiController = null;

    ProgressDialog progressDialog;

    TextView tv_create_account;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_import);

        userName = findViewById(R.id.et_login_username);
        userPhone = findViewById(R.id.et_login_number);
        tv_create_account = findViewById(R.id.tv_create_account);

        btLogin = findViewById(R.id.bt_login);

        mNetworkApiController = NetworkApiController.getInstance(LoginActivity.this);

        progressDialog = new ProgressDialog(LoginActivity.this);

        tv_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
                finish();

            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);


    }
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Checks.isInternetOn(LoginActivity.this)) {

                    if (!userName.getText().toString().equalsIgnoreCase("") &&
                            !userPhone.getText().toString().equalsIgnoreCase("")) {


                        progressDialog.setTitle("Authenticating");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        mNetworkApiController.login(userName.getText().toString(), userPhone.getText().toString());

                    } else {

                        Toast.makeText(LoginActivity.this, "Incomplete Info.", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    Checks.showAlert(LoginActivity.this,"No Internet Connection","OK");
                }
            }
        });



    }



    @Subscribe(threadMode = ThreadMode.MAIN)
        public void onLoginResponse(LoginEvent event)
    {

        if (progressDialog.isShowing())
            progressDialog.dismiss();

        if (event.isCode()) // (code is 200 / success)
        {

            userName.setText("");
            userPhone.setText("");

            startActivity(new Intent(LoginActivity.this,VerificationActivity.class).putExtra("digit",event.getUser_code()));
            finish();
        }
        else  // (code is 400 / failure)
        {

            Toast.makeText(LoginActivity.this, event.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }


}
