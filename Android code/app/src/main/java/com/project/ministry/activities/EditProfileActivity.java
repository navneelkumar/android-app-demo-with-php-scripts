package com.project.ministry.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.project.ministry.controller.NetworkApiController;
import com.project.ministry.model.events.UpdateProfileEvent;
import com.project.ministry.utitlty.HelperMethod;
import com.project.pidpool.R;
import com.project.ministry.netwrok.response.getProfile.Detail;
import com.project.ministry.utitlty.Checks;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class EditProfileActivity extends AppCompatActivity {


    ImageView iv_edit_profile_image;

    EditText et_edit_profile_username,et_edit_profile_phone, et_edit_profile_address;

    Button bt_edit_profile;

    Detail profileObject;

    Bitmap profileImage;

    NetworkApiController networkApiController;

    ProgressDialog progressDialog;

    LinearLayout edit_profile_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.edit_profile_toolbar);
        setSupportActionBar(toolbar);

        iv_edit_profile_image = findViewById(R.id.iv_edit_profile_image);

        edit_profile_back = findViewById(R.id.edit_profile_back);

        et_edit_profile_username = findViewById(R.id.et_edit_profile_username);
        et_edit_profile_phone = findViewById(R.id.et_edit_profile_phone);
        et_edit_profile_address = findViewById(R.id.et_edit_profile_address);

        bt_edit_profile = findViewById(R.id.bt_edit_profile);


        profileObject = NetworkApiController.controller.getUserProfileObject();

        profileImage = NetworkApiController.controller.getProfile_image();

        networkApiController = NetworkApiController.getInstance(EditProfileActivity.this);


        progressDialog = new ProgressDialog(EditProfileActivity.this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);


    }
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        getSupportActionBar().setTitle("");


        edit_profile_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        bt_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (Checks.isInternetOn(EditProfileActivity.this))
                {

                    if (et_edit_profile_username.getText().toString().equalsIgnoreCase("")
                            ||et_edit_profile_phone.getText().toString().equalsIgnoreCase("")||
                            et_edit_profile_address.getText().toString().equalsIgnoreCase(""))
                    {

                        Toast.makeText(EditProfileActivity.this, "Incomplete Info.", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {


                        progressDialog.setTitle("Syncing");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        networkApiController.updateUserProfile(profileObject.getUserId(),et_edit_profile_username.getText().toString(),
                                et_edit_profile_phone.getText().toString(),et_edit_profile_address.getText().toString());

                    }

                }
                else
                {
                    Checks.showAlert(EditProfileActivity.this,"No Internet Connection","OK");
                }

            }
        });


        if (profileImage!=null)
        {

            HelperMethod.showRoundImagePreviewBitmap(EditProfileActivity.this,profileImage,iv_edit_profile_image);
        }

        if (profileObject != null)
        {

            if (profileObject.getUsername() != null)
                et_edit_profile_username.setText(profileObject.getUsername());

            if (profileObject.getUserPhone() != null)
                et_edit_profile_phone.setText(profileObject.getUserPhone());

            if (profileObject.getAddress() != null)
                et_edit_profile_address.setText(profileObject.getAddress());



        }

    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateProfileResponse(final UpdateProfileEvent event)
    {

        if (progressDialog.isShowing())
            progressDialog.dismiss();

        if (event.isCode())
        {
            //code is 200

            networkApiController.getUserProfile();

            //Update profile in cache and database
            profileObject.setUsername(et_edit_profile_username.getText().toString());
            profileObject.setUserPhone(et_edit_profile_phone.getText().toString());
            profileObject.setAddress(et_edit_profile_address.getText().toString());

            NetworkApiController.controller.setUserProfileObject(profileObject);

            String jsonObject = HelperMethod.convertJavaToJson(profileObject);

            HelperMethod.setLocalProfile(jsonObject,EditProfileActivity.this);

            finish();

            Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show();

        }
        else
        {
            //code is 400

            Toast.makeText(this, event.getMessage(), Toast.LENGTH_SHORT).show();


        }




    }

}
